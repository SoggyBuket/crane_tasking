# -- Margie Lizie <3 -- 03/14/2024 -- #

# -- TODO 1 : make the .o files depend on .c files and .h files, cause they don't
# -  depend on .h files right now. Do this with $(DEPS)
# -- TODO 2 : have it so that this makefile creates the src/obj directory if it
# -  doesn't exist. Mostly cause it doesn't push up to gitlab

# -- this is a good makefile that I like. The main directories seen are an
# -  includes directory, a source directory and an objects directory. Includes
# -  is for .h files, src is for .c files, and obj is where .o files get stored
# -  after being compiled. Change around as much of this as you want, unless
# -  it's in a repo of mine, then as first pwibby pweebs :))

# -- where the directories are in relation to where we're calling make
INC_DIR = include
SRC_DIR = src
OBJ_DIR = src/obj

# -- basic flags for basic things
CC = clang
CFLAGS = -I$(INC_DIR) -Wall -g -std=c89
LDFLAGS = -lSDL3 -lSDL3_image

# -- when it finds files of those kinds in the prerequisites, search the
# -  corrosponding dir for them
vpath %.h $(INC_DIR)
vpath %.c $(SRC_DIR)
# vpath %.o $(OBJ_DIR)

# -- give all the source files in the src_dir directory
SRC_FILES := $(notdir $(wildcard $(SRC_DIR)/*.c))

# -- makes all of the names for the object files, and say that they should be
# -  in obj_dir
_OBJS := $(patsubst %.c,%.o,$(SRC_FILES))
OBJS := $(patsubst %,$(OBJ_DIR)/%,$(_OBJS))

# -- wildcard the include files
INC_FILES := $(wildcard $(INC_DIR)/*.h)
DEPS := $(patsubst %,$(INC_DIR)/%,$(INC_FILES))

# -- TODO 2

# -- build all of the .o files into obj_dir
# -- TODO 1
$(OBJ_DIR)/%.o : %.c
	$(CC) -c -o $@ $< $(CFLAGS)

# -- build the final thingy that will run the game (i can't think of the word
# -  right now) using all of those .o files we put into obj_dir
crane: $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)


# -- tell make not to try and compile the thing 'clean'
.PHONY: clean

# -- get rid of all of the .o files in obj_dir... and do something else idk
clean:
	-rm -f $(OBJ_DIR)/*.o *~ core $(INC_DIR)/*~

