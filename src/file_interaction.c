#include "file_interaction.h"

int file_write(const char *file_path, const char *data) {
    FILE *file;
    file = fopen(file_path, "w");
    if (file == NULL)
        return 0;
    fputs(data, file);
    fclose(file);
    return 1;
}

int file_append(const char *file_path, const char *data) {
    FILE *file;
    file = fopen(file_path, "a");
    if (file == NULL)
        return 0;
    fputs(data, file);
    fclose(file);
    return 1;
}

char *file_read(const char *file_path, uint amount) {
    FILE *file;
    file = fopen(file_path, "r");
    if (file == NULL)
        return NULL;

    char *str = calloc(amount, (sizeof (char)));
    if (fgets(str, amount, file) == NULL) {
        free(str);
        str = NULL;
    }

    fclose(file);

    return str;
}
