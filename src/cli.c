#include "cli.h"

void cli_display_usage(void) {
    printf("CRANE Usage: (not fully filled out yet)\n");
    printf("Options:\n");
    printf("    create\n");
    printf("        list <title> [<description>]\n");
    printf("        task <title> [<description>]\n");
    printf("    help: show this message\n");
}

void cli_parse(int argc, char **argv, CLIData *cli_data) {
    /* -- the first option in argv is the program name */
    uint index = 1;
    CLIOption option = cli_not_an_option;

    if (argc == 1 || strcmp(argv[index], "help") == 0) {
        ++index;
        option = cli_help;
    } else if (argc > 2 && strcmp(argv[index], "create") == 0) {
        ++index;
        if (strcmp(argv[index], "list") == 0) {
            ++index;
            option = cli_create_list;
        } else if (strcmp(argv[index], "task") == 0) {
            ++index;
            option = cli_create_task;
        }
    }

    cli_data->option = option;
    cli_data->argv_index = index;
}
