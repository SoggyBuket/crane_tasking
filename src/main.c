/* -- Margerine Lizie, 04/21/24 */

#include "basic.h"
#include "cli.h"

int main(int argc, char **argv) {
    printf("Here are the arguments you have given:\n");
    uint i;
    for (i = 1; i < argc; ++i)
        printf("%d: %s\n", i, argv[i]);

    CLIData cli_data;
    cli_parse(argc, argv, &cli_data);

    switch (cli_data.option) {
        case cli_not_an_option:
        case cli_help:
            cli_display_usage();
            break;
        case cli_create_list:
            printf("Create list!\n");
            break;
        case cli_create_task:
            printf("Create task!\n");
            break;
    }

    return 0;
}
