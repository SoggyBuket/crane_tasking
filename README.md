# CRANE Tasking

A great way for Creating a Repeated Avenue to Never do Enough (CRANE).

AKA this is a little command line tool that organizes your todo lists into plain text files. All of it is written in C and has not at all been tested on any machine other than mine, so be warned if you want to mess around with it.

This project currently seems like needlessly reinventing the wheel only because I want my data stored in plain text, but I don't care I'm stoked about it and I wanna do it.

Here is the current todo list I have for this project (simplified):
  - Implement general file io things
  - Specific file io for the way I want to lay out lists
  - Stored variables file
  - Command line options stuff (cause I've never done them in C before)
  - Creating a list (making a new file with heading & default data)
  - Creating a flags file with header & default flags
  - Creating a person file with header
  - Creating tasks within a list
  - Doing stuff with flags to tasks
  - Doing stuff with people to tasks
  - Have edit dates be working proper
  - Editing tasks
  - Meta data in flags syntax nailing
  - Basic Sorting
  - Move tasks to other indicies
  - Specific meta data pieces working
  - Advanced Sorting
  - Attaching tasks to other tasks
  - Moving tasks between lists
