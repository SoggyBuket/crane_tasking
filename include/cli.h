/* -- Margerine Lizie, 04/21/24 */

#ifndef CLI_H
#define CLI_H

#include "basic.h"
#include <string.h>

enum CLIOption {
    cli_not_an_option,
    cli_create_list,
    cli_create_task,
    cli_help
};

struct CLIData {
    enum CLIOption option;
    uint argv_index;
};

typedef enum CLIOption CLIOption;
typedef struct CLIData CLIData;

const char cli_create[2] = { "create", "c" };
const char cli_create_options[2] = {
    "list",
    "task"
};
const char cli_help[2] = { "help", "h" };

/* -- display the arguments that can be given to this program and how to use
 * -  them */
void cli_display_usage(void);
/* -- fills 'cli_data' with data from parsing argv */
/* -- TODO: this could for sure be implemented better */
void cli_parse(int argc, char **argv, CLIData *cli_data);

#endif
