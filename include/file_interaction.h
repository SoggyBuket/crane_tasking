/* -- Margerine Lizie 04/21/24 */

#ifndef FILE_INTERACTION_H
#define FILE_INTERACTION_H

#include "basic.h"

/* -- write 'data' to 'file_path' and return 0 if it failed */
int file_write(const char *file_path, const char *data);
/* -- append 'data' to 'file_path' and return 0 if it failed */
int file_append(const char *file_path, const char *data);
/* -- read the data in the file 'file_path' until 'amount'. returns NULL if the
 * -  read was unsuccessful */
/* -- NOTE: pointer returned must be freed to avoid memory leaks */
char *file_read(const char *file_path, uint amount);
/* -- read the line 'line_num' from 'file_path' */
/* char *file_readln(const char *file_path, uint line_num); */

#endif
